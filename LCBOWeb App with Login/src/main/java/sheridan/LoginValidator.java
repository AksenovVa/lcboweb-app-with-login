package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		return isValidSymbols(loginName) && isValidLength(loginName);
	}
	
	public static boolean isValidSymbols( String loginName ) throws NullPointerException{
		for (int i = 0; i < loginName.length(); ++i)
		{
			char cur = loginName.charAt(i);
			if (!((cur >= 'a' && cur <= 'z') || (cur >= 'A' && cur <='Z') || (cur >= '0' && cur <= '9')))
				return false;
		}
		return true;
	}
	public static boolean isValidLength( String loginName ) throws NullPointerException{
		return loginName.length( ) >= 6 ;
	}
}
