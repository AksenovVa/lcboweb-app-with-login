package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidSymbolsRegular( ) {
		assertEquals(true, LoginValidator.isValidSymbols( "Vlad" ) );
	}
	@Test (expected=NullPointerException.class)
	public void testIsValidSymbolsExceptional( ) {
		String a = null;
		assertEquals(true, LoginValidator.isValidSymbols( a ) );
		fail("type is not valid");
	}
	@Test
	public void testIsValidSymbolsBoundaryIn( ) {
		assertEquals(true, LoginValidator.isValidSymbols( "019" ) );
	}
	@Test
	public void testIsValidSymbolsBoundaryOut( ) {
		assertEquals(false, LoginValidator.isValidSymbols( "-" ));
	}
	@Test
	public void testIsValidLengthRegular( ) {
		assertEquals(true, LoginValidator.isValidLength( "VladAksenov" ) );
	}
	@Test (expected=NullPointerException.class)
	public void testIsValidLengthExceptional( ) {
		String a = null;
		assertEquals(true, LoginValidator.isValidLength( a ) );
		fail("type is not valid");
	}
	@Test
	public void testIsValidLengthBoundaryIn( ) {
		assertEquals(true, LoginValidator.isValidLength( "019012" ) );
	}
	@Test
	public void testIsValidLengthBoundaryOut( ) {
		assertEquals(false, LoginValidator.isValidLength( "01901" ));
	}

}
